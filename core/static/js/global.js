if (document.querySelector(".pageLoad")) {
    const fakeSlowPage = document.querySelector(".pageLoad");
    const documentBody = document.querySelector("body");
    let positionCount = 0;

    documentBody.style.pointerEvents = "none";

    setInterval(() => {
        fakeSlowPage.style.transform = `translateY(${positionCount}vh)`;
        positionCount += 10;
        console.log(positionCount);

        if (positionCount == 110) {
            documentBody.style.pointerEvents = "all";
            clearInterval();
        };

    }, 3000);
};