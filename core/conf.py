from core import db

# -- flask --
DEBUG = True # Debug server, turn this off while using it.
SECRET_KEY = "blablabla" # A secret key necessary for session encryption.
#SERVER_NAME = "whatisupdog.bank"

# -- flask-sqlalchemy --
DB_NAME = "data.db" # Database name
SQLALCHEMY_DATABASE_URI = f"sqlite:///{DB_NAME}" # Type of database you want it to use.
SQLALCHEMY_TRACK_MODIFICATIONS = False # Disabled to prevent overhead.

# -- flask-session --
SESSION_TYPE = "sqlalchemy"
SESSION_SQLALCHEMY = db
SESSION_COOKIE_NAME = "auth_key" # Name of authentication key 
SESSION_PERMANENT = True # Makes flask-sessions permanent 

# -- Misc --
GENERATE_TESTIMONIALS = False # do you want to generate new testimonials? (this will run twice if you are in debug, but it should still work)
OVERLAY = False # Puts an invisible overlay on the account page so it's hard for the scammer to edit the HTML.
FAKE_SLOW_PAGE = False # Makes the page look like it is slow by having a white box that moves down on the screen
QUICK_SIGN_OUT = False # signs the user out almost immediately after signing in
RANDOM_REFRESH = False # Refresh the page at 10-second intervals :)))
SHOW_WELCOME = True # Enables / disables the welcome splash screen on a fresh install.
MAX_NUM_AMOUNT = 1000000000 # The highest amount you can add to your balance, accounts and transactions.
MAX_NUM_LENGTH = len(str(MAX_NUM_AMOUNT)) # Don't change this, it gets the length of the max_num_amount so form submission works properly.
