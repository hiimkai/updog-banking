def router(app):
    from .views import views
    app.register_blueprint(views)
    return app